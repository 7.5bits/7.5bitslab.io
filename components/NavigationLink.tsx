import { UrlObject } from "url";
import Link from "next/link";
import { useRouter } from "next/router";

import styles from "./NavigationLink.module.css";
import { HTMLAttributes } from "react";

export interface LinkProps {
  id: string;
  text: string;
  url: UrlObject;
}

const activeLinkAttributes: HTMLAttributes<HTMLAnchorElement> = {
  className: styles.active,
  "aria-current": "page"
};

export default function NavigationLink(link: LinkProps) {
  const router = useRouter();
  const { url } = link;
  const isActive = url.pathname === router.pathname && JSON.stringify(url.query) === JSON.stringify(router.query);
  return (<Link href={link.url}><a {...isActive && activeLinkAttributes}>{link.text}</a></Link>);
}
