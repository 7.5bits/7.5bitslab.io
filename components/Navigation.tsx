import { UrlObject } from "url";
import { Component } from "react";

import { getSlug, GroupData, GroupTree, ProjectData } from "../api";

import styles from "./Navigation.module.css";
import NavigationLink, { LinkProps } from "./NavigationLink";

export interface LinkTree extends LinkProps {
  subitems: LinkTree[]
}

export const buildUrlObject = (slug: string[]): UrlObject => {
  const query = slug.length === 0 ? {} : { slug };
  return {
    pathname: "/[[...slug]]",
    query
  };
};

export const buildLinkProps = (groupOrProject: GroupData | ProjectData): LinkProps => {
  let slug = getSlug(groupOrProject);
  return {
    id: `group/${groupOrProject.id}`,
    text: groupOrProject.name,
    url: buildUrlObject(slug)
  };
};

export const buildLinkTree = (groupTree: GroupTree): LinkTree => {
  const creationDateMap = Object.fromEntries([
    ...groupTree.subgroups.map(group => [group.created_at, buildLinkTree(group)]),
    ...groupTree.projects.map(project => [project.created_at, {
      ...buildLinkProps(project),
      subitems: []
    }])
  ]);

  return {
    ...buildLinkProps(groupTree),
    subitems: Object.keys(creationDateMap)
      .sort()
      .reverse()
      .map(key => creationDateMap[key])
  };
};

const renderNavigationTree = (linkTree: LinkTree) => {
  return (
    <li key={linkTree.id} role="treeitem">
      <NavigationLink {...linkTree} />
      <ul className={styles.list} role="group">
        {linkTree.subitems.map(renderNavigationTree)}
      </ul>
    </li>
  );
};

interface Props {
  linkTree: LinkTree;
}

export class Navigation extends Component<Props, undefined> {
  render() {
    const { linkTree } = this.props;
    if (!linkTree) {
      return "Loading...";
    }

    return (
      <nav aria-label="menu" className={styles.menu}>
        <ul className={styles.list} role="tree">
          {renderNavigationTree(linkTree)}
        </ul>
      </nav>
    );
  }
}
