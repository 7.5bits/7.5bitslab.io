import { Component } from "react";
import { extractProjectTileProps, ProjectTile, ProjectTileProps } from "./ProjectTile";
import { GroupTree } from "../api";
import ReactMarkdown from "react-markdown";
import gfm from "remark-gfm";
import { PageProps } from "../pages/[[...slug]]";

import styles from "./GroupPage.module.css";

export interface GroupPageProps extends PageProps {
  description: string;
  projects: ProjectTileProps[];
}

export const buildGroupPageProps = (groupTree: GroupTree): GroupPageProps => {
  const { description, projects } = groupTree;
  return {
    pageType: "GroupPage",
    title: groupTree.name,
    description,
    projects: projects.map(extractProjectTileProps)
  };
};

export class GroupPage extends Component<GroupPageProps, undefined> {
  renderProjects() {
    const { projects } = this.props;
    if (!projects.length) {
      return;
    }

    return (
      <section>
        <h2>Projects</h2>
        <div className={styles.row}>{projects.map(p => (<ProjectTile key={p.id} {...p} />))}</div>
      </section>
    );
  }

  render() {
    const { description = "" } = this.props;
    return (
      <main>
        <ReactMarkdown plugins={[gfm]} children={description} />
        <hr />
        {this.renderProjects()}
      </main>
    );
  }
}
