import { Component, SyntheticEvent } from "react";
import Link from "next/link";

import { ProjectData } from "../api";
import { PageProps } from "../pages/[[...slug]]";

import styles from "./ProjectPage.module.css";

interface AudioSource {
  src: string;
  type: string;
}

export interface ProjectPageProps extends PageProps {
  path: string;
  rawFilesUrl: string;
  repositoryUrl: string;
  creationDate: string;
  lastChangeDate: string;
}

const audioTypes = {
  ogg: "audio/ogg; codecs=vorbis",
  mp3: "audio/mpeg"
};

const buildAudioSources = (props: ProjectPageProps): AudioSource[] => {
  const { path, rawFilesUrl } = props;
  return Object.entries(audioTypes).map(([extension, type]) => ({
    src: `${rawFilesUrl}/${path}.${extension}`,
    type
  }));
};

const formatDate = (formattedDateTime: string) => new Date(formattedDateTime).toISOString().split("T")[0];

export const buildProjectPageProps = (project: ProjectData): ProjectPageProps => {
  const { path, web_url: repositoryUrl, default_branch } = project;
  const rawFilesUrl = `${repositoryUrl}/-/raw/${default_branch}/`;
  const creationDate = formatDate(project.created_at)
  const lastChangeDate = formatDate(project.last_activity_at)
  return {
    pageType: "ProjectPage",
    title: project.name,
    path,
    rawFilesUrl,
    repositoryUrl,
    creationDate,
    lastChangeDate
  };
};

const stopOtherAudio = (event: SyntheticEvent) => {
  const allAudioElements = Array.from(document.getElementsByTagName("audio"));
  const otherAudioElements = allAudioElements.filter(element => element !== event.target);
  otherAudioElements.forEach(element => element.pause());
};

export class ProjectPage extends Component<ProjectPageProps, undefined> {
  render() {
    const { repositoryUrl } = this.props;
    const audioSources = buildAudioSources(this.props);
    return (
      <main>
        <table className={styles.projectDetails}>
          <tbody>
          <tr>
            <td>Created on:</td>
            <td>{this.props.creationDate}</td>
          </tr>
          <tr>
            <td>Last change on:</td>
            <td>{this.props.lastChangeDate}</td>
          </tr>
          <tr>
            <td>Repository:</td>
            <td><Link href={repositoryUrl}><a>{repositoryUrl}</a></Link></td>
          </tr>
          </tbody>
        </table>

        <audio controls onPlay={stopOtherAudio}>
          {audioSources && audioSources.map(props => (<source key={props.src} {...props} />))}
        </audio>
      </main>
    );
  }
}
