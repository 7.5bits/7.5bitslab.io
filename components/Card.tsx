import { Component } from "react";
import { UrlObject } from "url";
import Link from "next/link";

import styles from "./Card.module.css";

export interface CardProps {
  title: string;
  url: UrlObject;
}

export class Card extends Component<CardProps, undefined> {
  render() {
    const { title, url } = this.props;
    return (
      <section className={styles.card}>
        <h3 className={styles.cardTitle}><Link href={url}><a>{ title }</a></Link></h3>
      </section>
    );
  }
}