import { Component } from "react";

import { getSlug, ProjectData } from "../api";
import { buildUrlObject } from "./Navigation";
import { Card } from "./Card";

export interface ProjectTileProps {
  id: string;
  name: string;
  slug: string[];
}

export const extractProjectTileProps = (project: ProjectData): ProjectTileProps => {
  const { id, name } = project;
  return {
    id,
    name,
    slug: getSlug(project)
  };
};

export class ProjectTile extends Component<ProjectTileProps, undefined> {
  render() {
    const { name, slug } = this.props;
    return (
      <Card title={name} url={buildUrlObject(slug)} />
    );
  }
}
