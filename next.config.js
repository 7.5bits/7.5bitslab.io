module.exports = {
  trailingSlash: true,
  webpack(config) {
    config.node = {
      fs: "empty"
    };
    return config;
  }
};
