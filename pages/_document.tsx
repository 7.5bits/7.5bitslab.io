import Document, { DocumentContext, DocumentProps } from "next/document";
import { DocumentInitialProps } from "next/dist/next-server/lib/utils";

export default class CustomDocument extends Document {
  static async getInitialProps(ctx: DocumentContext): Promise<DocumentInitialProps> {
    const documentProps = await Document.getInitialProps(ctx);
    return {
      ...documentProps,
      locale: "en"
    } as DocumentProps;
  }
}
