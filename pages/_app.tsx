import { StrictMode } from "react";
import Head from "next/head";

import "../styles/globals.css";
import styles from "./_app.module.css";

import { LinkTree, Navigation } from "../components/Navigation";
import { useRouter } from "next/router";
import Link from "next/link";

export interface AppProps {
  title: string;
  linkTree: LinkTree;
}

function PermaLink(props: { title: string }) {
  const router = useRouter();
  const { pathname, query } = router;
  return (<Link href={{ pathname, query }}><a>{props.title}</a></Link>);
}

export default function App({ Component, pageProps }) {
  const { title, linkTree }: AppProps = pageProps;
  return (<StrictMode>
    <Head>
      <title>7.5bits | {title}</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>

    <Navigation linkTree={linkTree} />

    <div className={styles.page}>
      <header>
        <h1 className={styles.pageTitle}><PermaLink title={title} /></h1>
      </header>

      <Component {...pageProps} />
    </div>
  </StrictMode>);
}
