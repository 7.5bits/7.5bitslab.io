import { ParsedUrlQuery } from "querystring";
import { Component, ElementType } from "react";
import { GetStaticPaths, GetStaticProps } from "next";

import {
  buildPathFromSlug,
  fetchGroupTree,
  fetchRootGroup,
  findByPath,
  getPath,
  getSlug,
  GroupTree,
  isGroup,
  ProjectData
} from "../api";
import { buildGroupPageProps, GroupPage, GroupPageProps } from "../components/GroupPage";
import { buildProjectPageProps, ProjectPage, ProjectPageProps } from "../components/ProjectPage";
import { AppProps } from "./_app";
import { buildLinkTree } from "../components/Navigation";

const pageTypes = {
  GroupPage,
  ProjectPage
};

export type PageType = keyof typeof pageTypes;

export interface PageProps {
  pageType: PageType;
  title: string
}

interface PageMetadata {
  [key: string]: GroupPageProps | ProjectPageProps
}

interface Parameters extends ParsedUrlQuery {
  slug: string[];
}

const flattenTree = (groupTree: GroupTree): (GroupTree | ProjectData)[] => [
  groupTree,
  ...groupTree.projects,
  ...groupTree.subgroups
    .map(flattenTree)
    .reduce((a, b) => a.concat(b), [])
];

export const getStaticPaths: GetStaticPaths<Parameters> = async () => {
  const groupTree = await fetchGroupTree(await fetchRootGroup());
  const flatTree = flattenTree(groupTree);
  const slugs = flatTree.map(getSlug);
  const paths = slugs.map(slug => ({
    params: { slug }
  }));

  const buildMetadata = (projectOrGroup: GroupTree | ProjectData) => [
    getPath(projectOrGroup),
    isGroup(projectOrGroup) ?
      buildGroupPageProps(projectOrGroup as GroupTree) :
      buildProjectPageProps(projectOrGroup as ProjectData)
  ];

  const pageMetadata: PageMetadata = Object.fromEntries(flatTree.map(buildMetadata));

  const fs = require("fs");
  fs.writeFileSync("page-metadata.json", JSON.stringify(pageMetadata, null, 2));

  return {
    paths,
    fallback: false
  };
};

const findInTree = (groupTree: GroupTree, slug: string[]): GroupTree | ProjectData => {
  const path = buildPathFromSlug(slug);
  if (groupTree.full_path === path) {
    return groupTree;
  }

  const project = findByPath(groupTree.projects, path);
  if (project) {
    return project;
  }

  return groupTree.subgroups
    .map(i => findInTree(i, slug))
    .find(i => i !== undefined);
};

type Props = AppProps & PageProps;

export const getStaticProps: GetStaticProps<Props, Parameters> = async (context) => {
  const fs = require("fs");
  const pageMetadata: PageMetadata = JSON.parse(fs.readFileSync("page-metadata.json"));
  const { slug = [] } = context.params;
  const pageProps: PageProps = pageMetadata[buildPathFromSlug(slug)];

  const groupTree = await fetchGroupTree(await fetchRootGroup());
  const linkTree = buildLinkTree(groupTree);

  const props: Props = {
    ...pageProps,
    linkTree
  };

  return { props };
};

export default class Page extends Component<Props, undefined> {
  render() {
    const { props } = this;
    const PageComponent = pageTypes[props.pageType] as ElementType;
    return (<PageComponent key={props.title} {...props} />);
  }
}
