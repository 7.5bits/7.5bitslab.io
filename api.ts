export interface ProjectData {
  id: string;
  name: string;
  created_at: string;
  last_activity_at: string;
  default_branch: string;
  path: string;
  path_with_namespace: string;
  web_url: string;
}

export interface GroupData {
  id: string;
  name: string;
  created_at: string;
  description: string;
  full_path: string;
}

export interface GroupTree extends GroupData {
  projects: ProjectData[];
  subgroups: GroupTree[];
}

const readCacheFile = () => {
  if (process.browser) {
    return null;
  }

  const fs = require("fs");
  if (!fs.existsSync("cache.json")) {
    return null;
  }

  return JSON.parse(fs.readFileSync("./cache.json"));
};

const writeCacheFile = (cache: any) => {
  if (process.browser) {
    return;
  }

  const fs = require("fs");
  fs.writeFileSync("cache.json", JSON.stringify(cache, null, 2));
};

export const fetchData = async (url: string) => {
  const cacheFile = readCacheFile();
  const cachedData = cacheFile && cacheFile[url];
  if (cachedData) {
    return cachedData;
  }

  const response = await fetch(url);
  const data = await response.json();
  writeCacheFile({
    ...cacheFile,
    [url]: data
  });
  return data;
};

export const isGroup = (groupOrProject: GroupData | ProjectData) => (groupOrProject as GroupTree).full_path !== undefined;

export const getPath = (groupOrProject: GroupData | ProjectData): string =>
  isGroup(groupOrProject) ?
    (groupOrProject as GroupData).full_path : (groupOrProject as ProjectData).path_with_namespace;

export const getSlug = (groupOrProject: GroupData | ProjectData): string[] => {
  const slug = getPath(groupOrProject).split("/");

  // remove root group
  slug.shift();

  return slug;
};

export const buildPathFromSlug = (slug: string[]) => ["7.5bits", ...slug].join("/");

const groupApiUrl = (slug: string[]) => `https://gitlab.com/api/v4/groups/` + buildPathFromSlug(slug).replace(/\//g, "%2F");

export const fetchGroupData = (slug: string[]): Promise<GroupData> =>
  fetchData(groupApiUrl(slug));

export const fetchRootGroup = (): Promise<GroupData> =>
  fetchGroupData([]);

export const fetchProjects = async (groupSlug: string[]): Promise<ProjectData[]> => {
  const projects = await fetchData(groupApiUrl(groupSlug) + "/projects?per_page=100");
  return projects.filter(p => !p.name.endsWith(".winniehell.de"));
};

export const fetchSubgroups = (groupSlug: string[]): Promise<GroupData[]> =>
  fetchData(groupApiUrl(groupSlug) + "/subgroups?per_page=100");

export const fetchGroupTree = async (group: GroupData): Promise<GroupTree> => {
  const slug = getSlug(group);
  const subgroups = await fetchSubgroups(slug);
  const projects = await fetchProjects(slug);
  const subgroupTrees = await Promise.all(subgroups.map(fetchGroupTree));
  return {
    ...group,
    projects,
    subgroups: subgroupTrees
  };
};

export const findByPath = (projects: ProjectData[], projectPath: string) => projects.find(p => p.path_with_namespace === projectPath);

export const fetchProject = async (projectSlug: string[]): Promise<ProjectData> => {
  const groupSlug = projectSlug.slice(0, projectSlug.length - 1);
  const groupProjects = await fetchProjects(groupSlug);
  const projectPath = buildPathFromSlug(projectSlug);
  return findByPath(groupProjects, projectPath);
};
